const { readFile } = require('fs');
const { program } = require('commander');
const { rm } = require('./commands');

module.exports = {
    launch: data => {
        // rm subcommand
        program
            .command('rm <key> [file...]')
            .description('removes a key')
            .action((key, files, options) => {
                if (options.debug)
                    console.log(`Remove '${key}' for: `, {options, files, data});

                if (files.length > 0) {
                    files.forEach(filename => {
                        readFile(filename, (err, buffer) => {
                            if (err) {
                                console.error(err);
                            } else {
                                let data = JSON.parse(buffer);
                                rm(data, key, options);
                                if (options.print)
                                    console.log(JSON.stringify(data));
                            }
                        });
                    });
                    // TODO
                } else if (data) {
                    // TODO
                } else {
                    // no input!
                    program.outputHelp();
                    process.exit(1);
                }
            });

        // set subcommand
        program
            .command('set <key> <value> [file...]')
            .option('-v, --parse-value', 'Enables parsing integers, floats, arrays')
            .description('set key: value')
            .action((key, value, files, options) => {
                if (options.debug)
                    console.log(`Set '${key}' to '${value}' for: `, {options, files, data});

                if (files.length > 0) {
                    // TODO
                } else if (data) {
                    // TODO
                } else {
                    // no input!
                    program.outputHelp();
                    process.exit(1);
                }
            });

        // add shared options
        program.commands.forEach(command => {
            return command
                .option('-k, --parse-key', 'Enables parsing integers, floats, arrays')
                .option('-o, --out <file>', 'Out file')
                .option('-p, --print', 'Print to stdout')
                .option('-d, --debug', 'Enables debug');
        });

        program
            .version(process.env.npm_package_version)
            .parse();
    },
};