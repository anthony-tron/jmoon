module.exports = {
    rm: (object, key, {parseKey}) => {
        const property = () => `object.${key}`;

        if (parseKey) {
            if (key.startsWith('[')) {
                // '[0]', '[1]'
                object.splice(object.indexOf(eval(`object${key}`)), 1);
            }
            else if (key.endsWith(']')) {
                // 'array[0]', 'array[1]'
                const array = () => property().slice(0, property().indexOf('['));
                eval(array()).splice(eval(array()).indexOf(eval(property())), 1);
            }
            else {
                // 'property.subProperty'
                eval(`${property()} = undefined`);
            }
        } else {
            // 'property'
            object[key] = undefined;
        }
    },
};
