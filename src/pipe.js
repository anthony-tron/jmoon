const { fstat } = require('fs');

module.exports = {
    read: (stream) => {
        return new Promise((resolve, reject) => {
            let data = '';
            stream.on('data', chunk => data += chunk);
            stream.on('end', () => resolve(data));
            stream.on('error', reject);
            stream.setEncoding('utf-8');
            stream.resume();
        });
    },
    checkPipe: ({fd}) => {
        return new Promise((resolve, reject) => {
            fstat(fd, (error, stats) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(!error && stats.isFIFO());
                }
            });
        });
    }
};
