const { checkPipe, read } = require('./pipe');
const { launch } = require('./cli');

checkPipe(process.stdin)
    .then(piped => {
        if (piped) {
            read(process.stdin)
                .then(launch)
                .catch(console.error);
        } else {
            launch(false);
        }
    })
    .catch(console.error);
